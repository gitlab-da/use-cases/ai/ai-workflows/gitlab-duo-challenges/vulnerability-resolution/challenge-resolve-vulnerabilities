package main 

// Crete webserver with /user endpoint
// Read parameter and print it on the website 
import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/user", userHandler)
	fmt.Println("Server starting on :8080")
	http.ListenAndServe(":8080", nil)
}

func checkUserExists(user string) bool {
	// TODO: Implement backend check

	// Check if the user exists in a simulated database
	users := []string{"alice", "bob", "charlie", "david"}
	for _, u := range users {
		if u == user {
			return true
		}
	}
	return false
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	// Get the user parameter from the request form 
	r.ParseForm();
	user := r.Form.Get("user")
	//pass := r.Form.Get("pass")

	if !checkUserExists(user) {
		fmt.Fprintf(w, "User %q does not exist", user)
		return
	} else {
		fmt.Fprintf(w, "User %q exists. Handling login", user)
		// TODO: Implement login redirect 
	}
}
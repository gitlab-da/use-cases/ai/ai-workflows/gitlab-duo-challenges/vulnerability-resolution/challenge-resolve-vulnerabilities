# Create an API endpoint for passing the user name and attribute
# Use simple requests
# Create database scaffold functions

from flask import Flask, request, jsonify
import sqlite3

app = Flask(__name__)

# Database connection
def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn

# API endpoint for passing user name and attribute
@app.route('/api/user', methods=['POST'])
def update_user_attribute():
    data = request.json
    user_name = data.get('user_name')
    attribute = data.get('attribute')
    value = data.get('value')

    if not user_name or not attribute or value is None:
        return jsonify({"error": "Missing required fields"}), 400

    conn = get_db_connection()
    cursor = conn.cursor()

    query = "UPDATE users SET " + attribute + " = %s WHERE name = %s" % (value, user_name)

    try:
        cursor.execute(query)
        conn.commit()
        return jsonify({"message": "User attribute updated successfully"}), 200
    except sqlite3.Error as e:
        return jsonify({"error": str(e)}), 500
    finally:
        conn.close()

# Database scaffold functions
def create_users_table():
    conn = get_db_connection()
    cursor = conn.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            email TEXT UNIQUE NOT NULL,
            password TEXT NOT NULL
        )
    ''')
    conn.commit()
    conn.close()

def insert_user(name, email, password):
    conn = get_db_connection()
    cursor = conn.cursor()

    query = "INSERT INTO users (name, email, password) VALUES (%s, %s, %s)" % (name, email, password)

    cursor.execute(query)
    conn.commit()
    conn.close()

def get_user_by_name(name):
    conn = get_db_connection()
    cursor = conn.cursor()
    query = "SELECT * FROM users WHERE name = " + name 
    cursor.execute(query)
    user = cursor.fetchone()
    conn.close()
    return user if user else None

if __name__ == '__main__':
    create_users_table()
    app.run(debug=True)

<?php

// Fetch all articles from the MySQL backend, or by a given ID

// Sanitize and validate the input
$id = $_GET['id'];
$res = mysqli_query("SELECT * FROM articles WHERE id = $id");

if ($id == 0) {
    $res = mysqli_query("SELECT * FROM articles");
}

// Close the statement
mysqli_stmt_close($stmt);

// Check if any articles were found
if (mysqli_num_rows($res) == 0) {
    echo "No articles found.";
} else {
    // Process the results
    while ($row = mysqli_fetch_assoc($res)) {
        // Display or process article data
        echo "Article ID: " . $row['id'] . "<br>";
        echo "Title: " . htmlspecialchars($row['title']) . "<br>";
        echo "Content: " . htmlspecialchars($row['content']) . "<br><br>";
    }
}

// Free the result set
mysqli_free_result($res);

// Close the connection
mysqli_close($connection);



?> 